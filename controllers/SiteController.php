<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\SiteMenu;
use app\models\User;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	public function beforeAction($action)
{
		if (parent::beforeAction($action)) {
			$this->layout = Yii::$app->getRequest()->getCookies()->getValue('theme', 'ninja');
			if (!Yii::$app->getRequest()->getCookies()->has('theme')) 
			{			
				Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
					'name' => 'theme',
					'value' => 'ninja',
					'expire' => time() + 86400 * 365,
				]));
			}
			return true;  // or false if needed
		} else {
			return false;
		}
	}
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

		
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	/**
     * Login action.
     *
     * @return Response|string
     */
    public function actionUser()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $model->password = '';
		return $this->render('user', [
            'model' => $model,
        ]);
    }
	
	public function actionChange()
    {
		if (Yii::$app->getRequest()->getCookies()->has('theme')) 
		{
			$value = (Yii::$app->getRequest()->getCookies()->get('theme') == "zeiss") ? "ninja" : "zeiss";
			Yii::$app->getResponse()->getCookies()->remove('theme');
			Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
				'name' => 'theme',
				'value' => $value,
				'expire' => time() + 86400 * 365,
			]));
			
		}
		else
		{			
			Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
				'name' => 'theme',
				'value' => 'ninja',
				'expire' => time() + 86400 * 365,
			]));
		}
        return $this->redirect(Yii::$app->request->referrer);
    }
	
	public function actionAddAdmin() {
		$model = User::find()->where(['username' => 'kisina'])->one();
		if (empty($model)) {
			$user = new User();
			$user->username = 'kisina';
			$user->email = 'marj-erokhova@yandex.ru';
			$user->setPassword('kisina');
			$user->generateAuthKey();
			if ($user->save()) {
				echo 'good';
			}
		}
	}
}


