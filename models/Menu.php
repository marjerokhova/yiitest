<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

class Menu extends ActiveRecord
{

	private static function getMenuItems()
    { 
		$items = [];
		$query = Menu::find();
		$menu = $query->all();
        
        return $menu;
    }
    
    /*
     * @inheritdoc
     */
    public static function viewMenuItems()
    {        
        $menu = self::getMenuItems();
        if ( empty($menu) ) { return; }
        
        foreach ($menu as $item)
        {
            $result[] = [
                'label' => $item->name,
                'url' => [$item->url]
            ];
        }
        $result[] = '<li>'
            .Html::beginForm(['/site/change'], 'post')
            . Html::submitButton(
                    'Смена',
                    ['class' => 'btn logout']
                )
            . Html::endForm()
			.'</li>';
        return $result;
    }
}
