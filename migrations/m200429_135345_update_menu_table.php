<?php

use yii\db\Migration;

/**
 * Class m200429_135345_update_menu_table
 */
class m200429_135345_update_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		 Yii::$app->db->createCommand()->batchInsert('menu', ['name', 'url'], [
            ['Пункт меню', '/site/index'],
			['ЛК', '/site/user'],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200429_135345_update_menu_table cannot be reverted.\n";

        return false;
    }
    */
}
