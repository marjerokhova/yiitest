<?php

use yii\db\Migration;

/**
 * Class m200429_135858_update_user_table
 */
class m200429_135858_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		 Yii::$app->db->createCommand()->batchInsert('user', ['username', 'auth_key', 'password_hash', 'email', 'status', 'created_at', 'updated_at'], [
            ['kisina', 'L0QMO0RUyIskR-4cg7oXAzCBJFxKrgWY','$2y$13$xc2px5Uwjwx9SyMPCCoq1u5Vyg08X4c0QnaLP34XMwa4rhnXCpcD2', 'marj-erokhova@yandex.ru', 10, 1588018544, 1588018544],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200429_135858_update_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200429_135858_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
