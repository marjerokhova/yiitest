<?php

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class ZeissAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/src/zeiss/';

    public $css;
    public $js;

    public function init()
    {
        $min = YII_ENV_DEV ? '' : '.min';
        $this->css = [
			'plugin/bootstrap/css/bootstrap.min.css',
			'plugin/bootstrap/css/bootstrap-theme.min.css',
			'plugin/nprogress/nprogress.css',
            'styles/style-black.min.css',
        ];
        $this->js = [
			'scripts/jquery.min.js',
			'plugin/bootstrap/js/bootstrap.min.js',
			'plugin/nprogress/nprogress.js',
            'scripts/main.min.js',
        ];

        // �������� �� ���� ����� Bootstrap � Jquery
        Yii::$app->assetManager->bundles = [
            'yii\bootstrap\BootstrapAsset' => [
                'sourcePath' => $this->sourcePath,
                'css' => $this->css,
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'sourcePath' => $this->sourcePath,
                'js' => $this->js,
            ],
            'yii\web\JqueryAsset' => [
                'sourcePath' => $this->sourcePath,
                'js' => $this->js,
            ],
        ];
    }
}