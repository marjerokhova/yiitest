<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class ZeissAppAsset
 * @package app\assets
 */
class ZeissAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css'
    ];
    public $js = [
    ];
    public $depends = [
		'app\assets\ZeissAsset',
    ];
}