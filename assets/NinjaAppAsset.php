<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class NinjaAppAsset
 * @package app\assets
 */
class NinjaAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css'
    ];
    public $js = [
    ];
    public $depends = [
		'app\assets\NinjaAsset',
    ];
}