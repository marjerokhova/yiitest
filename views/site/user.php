<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'ЛК';
$this->params['breadcrumbs'][] = $this->title;
?><h1><?= Html::encode($this->title) ?></h1><?php
if  (Yii::$app->user->isGuest)
{
?>

<div class="site-login">
   

    <p>Форма авторизации</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Логин'); ?>

        <?= $form->field($model, 'password')->passwordInput()->label('Пароль'); ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ])->label('Запомнить меня'); ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="col-lg-offset-1" style="color:#999;">
        Попробуйте: в БД есть пользователь kisina(kisina)
    </div>
</div>
<?php
}else
{?>
<div class="site-logout">
	Привет <?= Yii::$app->user->identity->username ?>!
	
	<?=
                Html::beginForm(['/site/logout'], 'post')
                .Html::submitButton(
                    'Выйти',
                    ['class' => 'btn btn-primary']
                )
                .Html::endForm()
                
	?>
  
</div>
<?php 
}